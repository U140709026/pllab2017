use strict;
use warnings;

my $x = "I`m a string";
print "\$x is a scalar variable. \$x = $x\n";

$x = 45;
print "\$x is a scalar variable . \$x = $x\n";

my @arr = ("ali","mehmet","deniz");
print "@arr\n";
my $a = @arr;
print "size of \@arr = $a\n";
print "size of \@arr = " . scalar @arr . "\n";

my %grades = ("ali" => 60, "mehmet" =>45, "deniz" =>89);
print $arr[1] . "`s grade is " . $grades{$arr[1]} . "\n";

my $dna = "ATGCCCATTGAC";
my $pattern1 = "GCC";
my $pattern2 = "TTT";

if ($dna =~ /$pattern1/) { print "$dna does not contain $pattern1.\n"; }
else { print "$dna does not contain $pattern1.\n"; }

if ($dna =~ /$pattern2/) { print "$dna does not contain $pattern2.\n"; }
else { print "$dna does not contain $pattern2.\n"; }

print "Old dna : $dna\n";
$dna =~ s/AT/at/g;
print " New dna :$dna\n";